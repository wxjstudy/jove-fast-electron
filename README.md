## 开发

```bash
# 建议不要直接使用 npm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 如果安装失败，可以配置镜像地址后使用cnpm/npm尝试单独安装electron相关依赖
# 配置electron镜像地址
npm config set registry http://registry.npm.taobao.org/
npm config set electron_mirror="https://npm.taobao.org/mirrors/electron/"
npm config set electron_builder_binaries_mirror="http://npm.taobao.org/mirrors/electron-builder-binaries/"

# 安装 electron
npm install electron --save-dev

# 安装 electron 管理开发者工具
npm install electron-devtools-installer

# 安装 electron 持久化数据存储库
npm install electron-store

# 安装 electron 打包和构建
npm install vue-cli-plugin-electron-builder

# 本地启动
npm run dev
``` 

登录:admin/123456

## 打包

```bash
npm run electron:build

```
或进入项目bin目录执行build.bat
打包成功后会在dist_electron中生成了exe文件，点击安装即可。

内存溢出解决参考：
https://blog.csdn.net/Claudia_c/article/details/123183201

## 环境要求

```bash
node: 14.x
npm: 6.x
electron: 26.x
```

## 对应后端
[https://gitee.com/wxjstudy/jove-fast](https://gitee.com/wxjstudy/jove-fast)

## 效果
![输入图片说明](doc/01.png)
![输入图片说明](doc/02.png)
![输入图片说明](doc/03.png)

## 摸鱼交流
本人普通码农一枚，产生了想搞开源项目的想法，虽能力有限，但竭尽所能，说干就干~~
欢迎大家进群交流，本项目将一直开源。
点击链接加入群聊【Java/Vue摸鱼交流群】：
[![加入QQ群](https://img.shields.io/badge/603446086-blue.svg)](https://jq.qq.com/?_wv=1027&k=Y2XSJ0BC) ![输入图片说明](doc/preview/mobile01.jpgmobile01.jpg)
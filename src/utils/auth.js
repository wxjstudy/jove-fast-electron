const TokenKey = 'Admin-Token'

const ExpiresInKey = 'Admin-Expires-In'

export function getToken() {
  return localStorage.getItem(TokenKey)
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return localStorage.removeItem(TokenKey)
}

export function getExpiresIn() {
  return localStorage.getItem(ExpiresInKey) || -1
}

export function setExpiresIn(time) {
  return localStorage.setItem(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return localStorage.removeItem(ExpiresInKey)
}
